package hello.world.hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {
	@RequestMapping("/callapi1")
	public String index() {
        
        System.out.println("!!!!! BEFORE callAPI1");
        String value = callAPI1();
        System.out.println("!!!!! AFTER callAPI!");
        return value;
        
    }
    private static String callAPI1()
{
    System.out.println("!!!!! HELLO FROM callAPI1");
    try{
        final String uri = "https://jsonplaceholder.typicode.com/todos/1";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        System.out.println(result);
        System.out.println(result.toString());
        return result.toString();
    }catch(Exception e)
    {
        return e.getMessage()+"<br><br>"+e.toString()+"<br><br>"+e.getStackTrace().toString();
    }
}

}